/*
 ************************************************************************************
 * Copyright (C) 2022 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, $ */

(function () {
  enyo.kind({
    kind: 'OB.UI.ModalAction',
    name: 'CPSCT.UI.cashreceiveModal',
    hideCloseButton: false,
    autoDismiss: false,
    closeOnEscKey: false,
    bodyContent: {
      components: [{
        kind: 'Scroller',
        classes: 'obUiModalReturnReceipt-body-scroller',
        thumb: true,
        horizontal: 'hidden',
        components: [
          {
            name: 'attributes'
          }
        ]
      }]
    },
    bodyButtons: {
      components: [
        
      ]
    },
    executeOnShow: function () {
      var me = this;
      me.$.bodyContent.$.attributes.destroyComponents();
      var headerObj = new Object();
      headerObj.terminalLbl = OB.I18N.getLabel('cpsct_sourceterminal');
      headerObj.amountLbl = OB.I18N.getLabel('POSS_AmountOfCash');
      var headerline = me.$.bodyContent.$.attributes.createComponent({
           name: 'line_' + '-1',
           kind: 'OB.UI.CashTransferredHeader',
           newAttributes: headerObj,
           classes:
                 'obUiModalReceiptLinesProperties-scroller-attributes-obUiPropertyEditLine'
      });
      headerline.$.cpsctSourceTerminalLbl.setContent(headerObj.terminalLbl);
      headerline.$.cpsctAmountLbl.setContent(headerObj.amountLbl);
      
      var lineNum = 0;
      _.each(me.args.txList, function(txn){
          var editline = me.$.bodyContent.$.attributes.createComponent({
             name: 'line_' + lineNum,
             kind: 'OB.UI.CashTransferredList',
             newAttributes: txn,
             classes:
                    'obUiModalReceiptLinesProperties-scroller-attributes-obUiPropertyEditLine'
          });
          editline.$.cpsctSourceTerminal.setContent(txn.sourceTerminal);
          editline.$.cpsctSourceTerminalId.setContent(txn.sourceTerminalId);
          editline.$.cpsctAmount.setContent(txn.amount);
          editline.$.cpsctId.setContent(txn.id);
          editline.$.cpsctId.hide();
          editline.$.cpsctSourceTerminalId.hide();
          lineNum = lineNum + 1;   
      });
      me.$.bodyContent.$.attributes.render();
    },
    executeOnHide: function () {
      
    },
    initComponents: function () {
      this.inherited(arguments);
      this.attributeContainer = this.$.bodyContent.$.attributes;
      this.$.header.setContent("Cash Receive");
    }
  });

  OB.UI.WindowView.registerPopup('OB.OBPOSCashMgmt.UI.CashManagement', {
    kind: 'CPSCT.UI.cashreceiveModal',
    name: 'CPSCT.UI.cashreceiveModal'
  });
  
  enyo.kind({
  name: 'OB.UI.CashTransferredList',
  kind: 'OB.UI.SelectButton',
  handlers: {
      ontap: "tapped",
  },
  style:
    'border-bottom: 1px solid #cccccc; background-color: white; text-align: center; color: black; padding-top: 9px;',
  components: [
    {
      name: 'cpsctSourceTerminal',
      classes: 'span2',
      style: 'line-height: 35px; font-size: 17px; width: 150px;'
    },{
      name: 'cpsctSourceTerminalId',
      classes: 'span2',
      style: 'line-height: 35px; font-size: 17px; width: 150px;'
    },
    {
      name: 'cpsctAmount',
      classes: 'span4',
      style: 'line-height: 35px; font-size: 17px; width: 320px;'
    },
    {
      name: 'cpsctId',
      classes: 'span4',
      style: 'line-height: 35px; font-size: 17px; width: 100px;'
    },
    {
      style: 'clear: both;'
    }
  ],
  tapped: function() {
    var me = this;
    me.owner.owner.owner.owner.currentPayment.amount = me.$.cpsctAmount.getContent();
    var query = "insert into CPSCT_CashTransfer(id, sourceTerminal, destinationTerminal, amount, transferType, status, ctid) values"
                + "('" + me.$.cpsctId.getContent() + "',"
                +  "'" + me.$.cpsctSourceTerminalId.getContent() + "',"
                + "'" +  OB.MobileApp.model.get('terminal').id + "',"
                + "'" +  me.$.cpsctAmount.getContent()  + "',"
                + "'" + this.owner.owner.owner.args.args.model.get('type') + "',"
                + "'" +  'PROCESSED' + "'," 
                + "'" + OB.UTIL.get_UUID() + "'"                
                + " );"
    OB.Dal.queryUsingCache(OB.Model.CPSCT_CashTransfer, query, [], function (success) {
       me.owner.owner.owner.args.args.inherited(me.owner.owner.owner.args.origArguments);
    });  
    me.owner.owner.owner.hide();
   },
  initComponents: function() {
    this.inherited(arguments);
  }
  });

 enyo.kind({
  name: 'OB.UI.CashTransferredHeader',
  style:
    'border-bottom: 1px solid #cccccc; background-color: lavender; text-align: center; color: black; padding-top: 9px;',
  components: [
    {
      name: 'cpsctSourceTerminalLbl',
      classes: 'span2',
      style: 'line-height: 35px; font-size: 17px; width: 200px;'
    },
    {
      name: 'cpsctAmountLbl',
      classes: 'span4',
      style: 'line-height: 35px; font-size: 17px; width: 250px;'
    },
    {
      style: 'clear: both;'
    }
  ],
  initComponents: function() {
    this.inherited(arguments);
  }
 });
}());
