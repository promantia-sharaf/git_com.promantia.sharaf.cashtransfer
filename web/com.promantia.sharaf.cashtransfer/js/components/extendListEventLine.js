/*
 ************************************************************************************
 * Copyright (C) 2022 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, $ */

OB.OBPOSCashMgmt.UI.ListEventLine.extend({
    tap: function() {
       var origArguments = arguments;
       if (this.model.get('type') === 'drop' && this.model.get('isCashTransfer')) {
            var me = this;
            var i = 0;
            var totalCash = 0;
            var currentAmt = me.owner.owner.owner.owner.owner.owner.owner.currentPayment.amount;
            for(i = 0; i < me.owner.owner.owner.owner.owner.owner.owner.model.get('payments').models.length; i++) {
                var payment = me.owner.owner.owner.owner.owner.owner.owner.model.get('payments').models[i];
                if (payment.get('searchKey') != null && payment.get('searchKey').toLowerCase().includes('cash')) {
                    totalCash = payment.get('total');
                }
            }
            if (totalCash < currentAmt) {
                OB.UTIL.showConfirmation.display('Available cash is : ' + totalCash + '. Transfer amount should be less than available cash');
            } else {
                new OB.DS.Process('com.promantia.sharaf.cashtransfer.service.DestinationTerminalListService').exec({
                   client: OB.MobileApp.model.get('terminal').client,
                   org: OB.MobileApp.model.get('terminal').organization,
                   pos: OB.MobileApp.model.get('terminal').id,
                   paymentMethod : me.model.get('paymentmethod')
                }, function(data) {
                   if (data && !data.exception) {
                     OB.MobileApp.view.$.containerWindow.showPopup(
                        'CPSCT.UI.cashtransferModal',
                      {
                         args: me,
                         origArguments: origArguments,
                         dstList: data 
                      }
                     );                 
                   } else {
                     OB.UTIL.showConfirmation.display(OB.I18N.getLabel('cpsct_faileddstlist'));
                   }
                }, function(error) {
                     OB.UTIL.showConfirmation.display(OB.I18N.getLabel('cpsct_faileddstlist'));
                });
             }
       } else if (this.model.get('type') === 'deposit' && this.model.get('isCashTransfer')){
              var me = this;
              new OB.DS.Process('com.promantia.sharaf.cashtransfer.service.CashTransactionListService').exec({
                   pos: OB.MobileApp.model.get('terminal').id,
                   org: OB.MobileApp.model.get('terminal').organization
              }, function(data) {
                   if (data && !data.exception) {
                     OB.MobileApp.view.$.containerWindow.showPopup(
                        'CPSCT.UI.cashreceiveModal',
                      {
                          args: me,
                          origArguments: origArguments,
                          txList: data 
                      }
                     );                 
              } else {
                   OB.UTIL.showConfirmation.display(OB.I18N.getLabel('cpsct_failedtxlist'));
                 }
              }, function(error) {
                     OB.UTIL.showConfirmation.display(OB.I18N.getLabel('cpsct_failedtxlist'));
                 }
              );
       } else {
         this.inherited(arguments);
       }
       this.doHideThisPopup();
    }
});