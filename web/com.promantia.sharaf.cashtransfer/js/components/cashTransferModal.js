/*
 ************************************************************************************
 * Copyright (C) 2022 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, $ */

(function () {
  enyo.kind({
    kind: 'OB.UI.ModalAction',
    name: 'CPSCT.UI.cashtransferModal',
    hideCloseButton: true,
    autoDismiss: false,
    closeOnEscKey: false,
    bodyContent: {
      components: [{
        name: 'terminalLabelMain',
        classes: 'properties-label',
        components: [{
          style: 'padding: 5px 5px 5px 5px; font-size: 20px;  margin-left: 0px;',
          name: 'terminalLabel'
        }]
      }, {
        name: 'TerminalOption',
        components: [{
          kind: 'OB.UI.List',
          name: 'terminalOptionsList',
          tag: 'select',
          classes: 'modal-dialog-profile-combo',
          renderEmpty: enyo.Control,
          renderLine: enyo.kind({
            kind: 'enyo.Option',
            initComponents: function () {
              this.inherited(arguments);
              this.setValue(this.model.get('id'));
              this.setContent(this.model.get('_identifier'));
            }
          })
        }]
      }, {
        tag: "br"
      }]
    },
    bodyButtons: {
      components: [{
        kind: 'CPSCT.UI.cashtransferOk'
      },
      {
        kind: 'CPSCT.UI.cashtransferCancel'
      }]
    },
    executeOnShow: function () {
      var dstTerminalList = new Backbone.Collection();
      this.$.bodyContent.$.terminalOptionsList.setCollection(dstTerminalList);
      var list = new Array();
      _.each(this.args.dstList, function(terminal){
        var newTerminal = new Object();
        newTerminal.id = terminal.id;
        newTerminal._identifier = terminal.terminalIdentifier;
        list.push(newTerminal);   
      });
             
      list.sort(function(a, b) { 
        aNum = a._identifier.match(/\d+/g);
        bNum = b._identifier.match(/\d+/g);
        if (!OB.UTIL.isNullOrUndefined(aNum[0]) && !OB.UTIL.isNullOrUndefined(bNum[0])) {
           return aNum[0] - bNum[0];
        } else {
           return a - b;        
        }
      });
      dstTerminalList.reset(list);
    },
    executeOnHide: function () {
      
    },
    initComponents: function () {

      this.inherited(arguments);
      enyo.forEach(this.newAttributes, function (natt) {
        this.$.bodyContent.$.attributes.createComponent(natt);
      }, this);
      this.$.header.setContent(OB.I18N.getLabel('CPSCT_CashTransferHeader'));
      this.$.bodyContent.$.terminalLabel.setContent(OB.I18N.getLabel('CPSCT_DestinationTerminal'));

    }
  });

  OB.UI.WindowView.registerPopup('OB.OBPOSCashMgmt.UI.CashManagement', {
    kind: 'CPSCT.UI.cashtransferModal',
    name: 'CPSCT.UI.cashtransferModal'
  });

  enyo.kind({
    kind: 'OB.UI.ModalDialogButton',
    name: 'CPSCT.UI.cashtransferOk',
    handlers: {
      ontap: "tapped",
    },
    initComponents: function () {
      this.inherited(arguments);
      this.setContent(OB.I18N.getLabel('OBMOBC_LblOk'));
    },
    tapped: function (inEvent, inSender) {
      if (OB.UTIL.isNullOrUndefined(this.owner.owner.$.bodyContent.$.terminalOptionsList.getValue())) {
        me.doHideThisPopup();
      } else {
          var me = this;
          var query = "insert into CPSCT_CashTransfer(id, sourceTerminal, destinationTerminal, amount, transferType, status, ctid) values"
                      + "('" + OB.UTIL.get_UUID() + "',"
                      +  "'" + OB.MobileApp.model.get('terminal').id + "',"
                      + "'" +  this.owner.owner.$.bodyContent.$.terminalOptionsList.getValue() + "',"
                      + "'" +  this.owner.owner.owner.currentPayment.amount + "',"
                      + "'" + me.owner.owner.args.args.model.get('type') + "',"
                      + "'" +  'INITIAL' + "'," 
                      + "'" + OB.UTIL.get_UUID() + "'"                       
                      + " );"
          OB.Dal.queryUsingCache(OB.Model.CPSCT_CashTransfer, query, [], function (success) {
             me.owner.owner.args.args.inherited(me.owner.owner.args.origArguments);  
             me.doHideThisPopup();
          }); 
      }           
    }
  });

  enyo.kind({
    kind: 'OB.UI.ModalDialogButton',
    name: 'CPSCT.UI.cashtransferCancel',
    handlers: {
      ontap: "tapped",
    },
    initComponents: function () {
      this.inherited(arguments);
      this.setContent(OB.I18N.getLabel('OBMOBC_LblCancel'));
    },
    tapped: function (inEvent, inSender) {
      this.doHideThisPopup();
    }
  });
}());
