/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, $ */


(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_TerminalLoadedFromBackend', function (args, callbacks) {
    new OB.DS.Process('com.promantia.sharaf.cashtransfer.service.CashTransferEvents').exec({
        client: OB.MobileApp.model.get('terminal').client,
        org: OB.MobileApp.model.get('terminal').organization,
        pos: OB.MobileApp.model.get('terminal').id,
        paymentMethod : ""
      }, function(data) {
         if (data && !data.exception) {
          _.each(OB.MobileApp.model.get('cashMgmtDepositEvents'), function(event) {
            _.each(data, function(cEvent) {
                if (event.id === cEvent.id && cEvent.type === 'deposit') {
                    event.isCashTransfer = true;
                }
            });          
          });
          _.each(OB.MobileApp.model.get('cashMgmtDropEvents'), function(event) {
            _.each(data, function(cEvent) {
                if (event.id === cEvent.id && cEvent.type === 'drop') {
                    event.isCashTransfer = true;
                }
            });          
          });                         
         } 
      }, function(error) {
 
      }
    );
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());
