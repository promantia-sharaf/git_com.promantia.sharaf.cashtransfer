/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, $ */


(function () {

OB.UTIL.HookManager.registerHook('OBMOBC_PreWindowOpen', function (args, callbacks) {
 if(args.context.name == 'OB.UI.MenuWindowItem.OBPOS_LblCloseCash') {
    var cashMgmtPending = false;
    var flag = false;
               new OB.DS.Process('com.promantia.sharaf.cashtransfer.service.CashTransactionListService').exec({
                   pos: OB.MobileApp.model.get('terminal').id,
                   org: OB.MobileApp.model.get('terminal').organization
              }, function(data) {
                   flag = true;
                   if (data && !data.exception) {
                    var destinationTerminal;
                    var currentTerminal = OB.MobileApp.model.get('terminal').searchKey;
                    for (var i=0; i<data.length; i++) {
                        destinationTerminal = data[i].destinationTerminal;
                        if(currentTerminal === destinationTerminal) {
                           cashMgmtPending = true;
                           OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'),"'Transfer In' Pending in this terminal, Please Do 'Cash Receive' in Cash Management for Pending 'Transfer In' Then Proceed with Cash-up"); 
                        }
                    }
              } else {
                   cashMgmtPending = false;
                 }
                if(cashMgmtPending) {
                  args.cancellation = true; 
                 } 
              if(flag) {
                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
              }
             }
           );   
 } else {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
}    
});
}());





























