/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, $ */


(function () {

  OB.UTIL.HookManager.registerHook('OBMOBC_PreWindowNavigate', function (args, callbacks) {
          var criteria = {
            whereClause: ' WHERE status= "INITIAL" OR status= "PROCESSED"'
          };    
    OB.Dal.removeAll(OB.Model.CPSCT_CashTransfer, criteria);
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);  
  });

}());