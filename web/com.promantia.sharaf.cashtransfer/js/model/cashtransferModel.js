/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB */

(function() {
  var cashtransfer = OB.Data.ExtensibleModel.extend({
    modelName: 'CPSCT_CashTransfer',
    tableName: 'CPSCT_CashTransfer',
    entityName: 'CPSCT_CashTransfer',
    source: '',
    local: true
  });
  cashtransfer.addProperties([
    {
      name: 'id',
      column: 'id',
      type: 'TEXT'
    },
    {
      name: 'sourceTerminal',
      column: 'sourceTerminal',
      type: 'TEXT'
    },
    {
      name: 'amount',
      column: 'amount',
      type: 'TEXT'
    },
    {
      name: 'destinationTerminal',
      column: 'destinationTerminal',
      type: 'TEXT'
    },
    {
      name: 'transferType',
      column: 'transferType',
      type: 'TEXT'
    },
    {
      name: 'status',
      column: 'status',
      type: 'TEXT'
    },
    {
      name: 'ctid',
      column: 'ctid',
      primaryKey: true,
      type: 'TEXT'
    }
  ]);
  OB.Data.Registry.registerModel(cashtransfer);
  OB.MobileApp.model.get('dataSyncModels').push({
      model: OB.Model.CPSCT_CashTransfer,
      criteria: {},
      className: 'com.promantia.sharaf.cashtransfer.CashTransferLoader'
  });
})();
