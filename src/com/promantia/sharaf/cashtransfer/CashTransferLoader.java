package com.promantia.sharaf.cashtransfer;

import java.math.BigDecimal;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.DataSynchronizationProcess.DataSynchronization;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.retail.posterminal.POSDataSynchronizationProcess;
import org.openbravo.service.json.JsonConstants;

@DataSynchronization(entity = "CPSCT_CashTransfer")
public class CashTransferLoader extends POSDataSynchronizationProcess {

  @Override
  public JSONObject saveRecord(JSONObject jsonRecord) throws Exception {
    OBPOSApplications sourcePos = OBDal.getInstance().get(OBPOSApplications.class,
        jsonRecord.optString("sourceTerminal"));
    OBPOSApplications dstPos = OBDal.getInstance().get(OBPOSApplications.class,
        jsonRecord.optString("destinationTerminal"));
    String status = jsonRecord.getString("status");
    Double amount = jsonRecord.getDouble("amount");

    CpsctCashTransfer ct = null;
    if (jsonRecord.has("id") && jsonRecord.getString("id") != null) {
      ct = OBDal.getInstance().get(CpsctCashTransfer.class, jsonRecord.get("id"));
      if (ct == null) {
        ct = new CpsctCashTransfer();
      }
    } else {
      ct = new CpsctCashTransfer();
    }
    ct.setSourceTerminal(sourcePos.getIdentifier());
    ct.setDestinationTerminal(dstPos.getIdentifier());
    ct.setAmount(BigDecimal.valueOf(amount));
    ct.setOrganization(sourcePos.getOrganization());
    ct.setStatus(status);

    OBDal.getInstance().save(ct);

    JSONObject jsonResponse = new JSONObject();
    jsonResponse.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    jsonResponse.put("result", "0");
    return jsonResponse;
  }

}
