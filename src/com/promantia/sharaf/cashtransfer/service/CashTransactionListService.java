package com.promantia.sharaf.cashtransfer.service;

import java.util.List;

import javax.servlet.ServletException;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.service.json.JsonConstants;

import com.promantia.sharaf.cashtransfer.CpsctCashTransfer;

public class CashTransactionListService extends JSONProcessSimple {

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    String organizationId, pos = null;

    JSONObject terminalObjJson = new JSONObject();
    try {
      if (!jsonsent.has("org") || jsonsent.getString("org") == null || !jsonsent.has("pos")
          || jsonsent.getString("pos") == null) {
        throw new OBException("Client/Organization/Payment Method Id is mandatory");
      } else {
        organizationId = jsonsent.getString("org");
        pos = jsonsent.getString("pos");
      }

      OBContext.setAdminMode(true);
      OBPOSApplications posObj = OBDal.getInstance().get(OBPOSApplications.class, pos);

      OBQuery<CpsctCashTransfer> criteria = OBDal.getInstance().createQuery(CpsctCashTransfer.class,
          " destinationTerminal = '" + posObj.getIdentifier() + "' and organization.id = '"
              + organizationId + "' and status = 'INITIAL'");
      JSONArray listJson = new JSONArray();
      List<CpsctCashTransfer> list = criteria.list();
      if (list.size() > 0) {
        for (CpsctCashTransfer transaction : list) {
          if (transaction.isActive()) {
            JSONObject txn = new JSONObject();
            txn.put("id", transaction.getId());
            txn.put("amount", transaction.getAmount());
            txn.put("sourceTerminal", transaction.getSourceTerminal());
            txn.put("destinationTerminal", transaction.getDestinationTerminal());
            txn.put("status", transaction.getStatus());

            OBQuery<OBPOSApplications> sourceTerminal = OBDal.getInstance().createQuery(
                OBPOSApplications.class,
                " terminalKey = '" + transaction.getSourceTerminal() + "'");
            txn.put("sourceTerminalId", sourceTerminal.list().get(0).getId());

            listJson.put(txn);
          }
        }
      }
      terminalObjJson.put(JsonConstants.DATA, listJson);
      terminalObjJson.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    } catch (Exception e) {
      terminalObjJson.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
      terminalObjJson.put(JsonConstants.RESPONSE_ERRORMESSAGE, e.getMessage());
    }
    OBContext.restorePreviousMode();
    return terminalObjJson;
  }
}
