package com.promantia.sharaf.cashtransfer.service;

import java.util.List;

import javax.servlet.ServletException;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.core.OBContext;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.retail.config.CashManagementEvents;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.service.json.JsonConstants;

public class CashTransferEvents extends JSONProcessSimple {
  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    String clientId, organizationId;

    JSONObject terminalObjJson = new JSONObject();
    try {
      if (!jsonsent.has("client") || jsonsent.getString("client") == null || !jsonsent.has("org")
          || jsonsent.getString("org") == null) {
        throw new OBException("Client/Organization Id is mandatory");
      } else {
        clientId = jsonsent.getString("client");
        organizationId = jsonsent.getString("org");
      }

      OBContext.setAdminMode(true);
      Client client = OBDal.getInstance().get(Client.class, clientId);
      Organization organization = OBDal.getInstance().get(Organization.class, organizationId);
      OBCriteria<CashManagementEvents> listCriteria = OBDal.getInstance()
          .createCriteria(CashManagementEvents.class);
      listCriteria.add(Restrictions.eq(OBPOSApplications.PROPERTY_CLIENT, client));
      listCriteria.add(Restrictions.eq(OBPOSApplications.PROPERTY_ORGANIZATION, organization));

      JSONArray listJson = new JSONArray();
      List<CashManagementEvents> list = listCriteria.list();
      if (list.size() > 0) {
        for (CashManagementEvents item : list) {
          if (item.isActive() && Boolean.TRUE.equals(item.isCpsctIstransfer())) {
            JSONObject json = new JSONObject();
            json.put("id", item.getId());
            json.put("type", item.getEventtype().equals("OUT") ? "drop" : "deposit");

            listJson.put(json);
          }
        }
      }
      terminalObjJson.put(JsonConstants.DATA, listJson);
      terminalObjJson.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    } catch (Exception e) {
      terminalObjJson.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
      terminalObjJson.put(JsonConstants.RESPONSE_ERRORMESSAGE, e.getMessage());
    }
    OBContext.restorePreviousMode();
    return terminalObjJson;
  }
}
