package com.promantia.sharaf.cashtransfer.service;

import java.util.List;

import javax.servlet.ServletException;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.retail.posterminal.TerminalTypePaymentMethod;
import org.openbravo.service.json.JsonConstants;

public class DestinationTerminalListService extends JSONProcessSimple {

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    String clientId, organizationId, paymentMethodId, pos = null;

    JSONObject terminalObjJson = new JSONObject();
    try {
      if (!jsonsent.has("client") || jsonsent.getString("client") == null || !jsonsent.has("org")
          || jsonsent.getString("org") == null || !jsonsent.has("paymentMethod")
          || jsonsent.getString("paymentMethod") == null || !jsonsent.has("pos")
          || jsonsent.getString("pos") == null) {
        throw new OBException("Client/Organization/Payment Method Id is mandatory");
      } else {
        clientId = jsonsent.getString("client");
        organizationId = jsonsent.getString("org");
        paymentMethodId = jsonsent.getString("paymentMethod");
        pos = jsonsent.getString("pos");
      }

      OBContext.setAdminMode(true);
      Client client = OBDal.getInstance().get(Client.class, clientId);
      Organization organization = OBDal.getInstance().get(Organization.class, organizationId);

      OBCriteria<OBPOSApplications> terminalListCriteria = OBDal.getInstance()
          .createCriteria(OBPOSApplications.class);
      terminalListCriteria.add(Restrictions.eq(OBPOSApplications.PROPERTY_CLIENT, client));
      terminalListCriteria
          .add(Restrictions.eq(OBPOSApplications.PROPERTY_ORGANIZATION, organization));

      JSONArray terminalListJson = new JSONArray();
      List<OBPOSApplications> terminalList = terminalListCriteria.list();
      if (terminalList.size() > 0) {
        for (OBPOSApplications terminal : terminalList) {
          if (terminal.isActive() && !terminal.getId().equals(pos)) {
            for (TerminalTypePaymentMethod paymentType : terminal.getObposTerminaltype()
                .getOBPOSAppPaymentTypeList()) {
              if (paymentType.getPaymentMethod().getId().equals(paymentMethodId)) {
                JSONObject terminalJson = new JSONObject();
                terminalJson.put("id", terminal.getId());
                terminalJson.put("terminalIdentifier", terminal.getTerminalKey());

                terminalListJson.put(terminalJson);
              }
            }
          }
        }
      }
      terminalObjJson.put(JsonConstants.DATA, terminalListJson);
      terminalObjJson.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    } catch (Exception e) {
      terminalObjJson.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
      terminalObjJson.put(JsonConstants.RESPONSE_ERRORMESSAGE, e.getMessage());
    }
    OBContext.restorePreviousMode();
    return terminalObjJson;
  }

}
